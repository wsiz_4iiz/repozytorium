﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QUIZ
{
    /// <summary>
    /// Logika interakcji dla klasy koniec.xaml
    /// </summary>
    public partial class koniec : Window
    {


        public koniec(Game gra)
        {

            InitializeComponent();

            Ranking rank = new Ranking();
            int poz = rank.getRanking(gra.nazwaGracza, gra.punkty);

            this.nickg.Content = gra.nazwaGracza;
            this.punkty.Content = gra.punkty;
            this.ranking.Content = poz.ToString();
        }

        private void bZakonczGre2_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void bOdNowa_Click(object sender, RoutedEventArgs e)
        {
            var window = new MainWindow();
            this.Close();

            window.Show();
        }
    }
}
