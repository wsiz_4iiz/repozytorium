﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QUIZ
{
    /// <summary>
    /// Logika interakcji dla klasy zasady.xaml
    /// </summary>
    public partial class autorzy : Window
    {
        public autorzy()
        {
            InitializeComponent();
            this.Title = "AUTORZY";
        }

        private void bPowrot2_Click(object sender, RoutedEventArgs e)
        {
            var window = new MainWindow();
            this.Hide();
            window.Show();
        }
    }
}
