﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QUIZ
{
    /// <summary>
    /// Logika interakcji dla klasy RankingList.xaml
    /// </summary>
    public partial class RankingList : Window
    {
        public RankingList()
        {
            InitializeComponent();

            Ranking rank = new Ranking();
            rank.getRanking();

            String text = "";

            int ile = rank.liczba_rekordow;
            if (ile > 10) { ile = 10; }
            for (int i = 0; i < ile; i++) {
                text += rank.rank[i].pozycja + ". " + rank.rank[i].nick + " - " + rank.rank[i].punkty + " pkt." + Environment.NewLine;    
            }
            this.rankList.Text = text;
        }

        private void bPowrot_Click(object sender, RoutedEventArgs e)
        {
            var window = new MainWindow();
            this.Hide();
            window.Show();
        }
    }
}
