﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUIZ
{
    class Ranking
    {

        public RankingPosition[] rank = new RankingPosition[10000];
        public int liczba_rekordow = 0;

        /// <summary> 
        /// Zapisuje ranking do pliku
        /// </summary> 
        public void saveResult() {
            FileStream fs = new FileStream(@"ranking.rank", FileMode.Truncate);
            StreamWriter sw = new StreamWriter(fs);
           // Trace.WriteLine("aaaaaa: "+rank.Length.ToString());
            for (int i = 0; i < this.liczba_rekordow; i++) {
                sw.WriteLine(rank[i].nick + ";" + rank[i].punkty.ToString() + ";" + rank[i].pozycja.ToString());
            }
            
            sw.Close();
            fs.Close();
        }

        /// <summary> 
        /// pobiera pozycje w rankingu, generuje nowy ranking
        /// </summary> 
        public int getRanking(string nick, int punkty)
        {

            int poz_do_zapisu = 1;
            int my_pos = 1;
            Boolean zapisano = false;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(@"ranking.rank");
            int licznik = 0;
            int licznik_lini = 0;
            //int pozycja = 1;
            while ((line = file.ReadLine()) != null)
            {
                licznik_lini++;
                if (!zapisano)
                {
                    if (punkty > Convert.ToInt32(line.Split(';')[1])){
                        rank[licznik] = new RankingPosition(nick, punkty, poz_do_zapisu);
                       // rank[licznik].pozycja = poz_do_zapisu;
                       // rank[licznik].punkty = punkty;
                        //rank[licznik].nick = nick;
                        licznik++;
                        zapisano = true;
                        my_pos = poz_do_zapisu;
                        poz_do_zapisu++;
                    }
                    
                }

                rank[licznik] = new RankingPosition(line.Split(';')[0], Convert.ToInt32(line.Split(';')[1]), poz_do_zapisu);
                poz_do_zapisu++;
                licznik++;
                //rank[licznik].pozycja = Convert.ToInt32(line.Split(';')[2]);
                //rank[licznik].punkty = Convert.ToInt32(line.Split(';')[1]);
                //rank[licznik].nick = line.Split(';')[0];

                
            }
            if (!zapisano) {
                rank[licznik] = new RankingPosition(nick, punkty, poz_do_zapisu);
                my_pos = poz_do_zapisu;
                licznik++;
            }

            //// rank[licznik].pozycja = poz_do_zapisu;
            // rank[licznik].punkty = punkty;
            // rank[licznik].nick = nick;

            file.Close();
            this.liczba_rekordow = licznik;
            saveResult();
            return my_pos;
        }

        public void getRanking()
        {

            int poz_do_zapisu = 1;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(@"ranking.rank");
            int licznik = 0;
            while ((line = file.ReadLine()) != null)
            {
                
                rank[licznik] = new RankingPosition(line.Split(';')[0], Convert.ToInt32(line.Split(';')[1]), poz_do_zapisu); 
                licznik++;
                poz_do_zapisu++;
               
            }
            file.Close();
            this.liczba_rekordow = licznik;
            saveResult();
        }



    }
}
