﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUIZ
{
    public class Questions
    {
        public Question[] allQuestions = new Question[500];
        public Question[] selectedQuestions = new Question[10];
        public int allQuestionsCounter;

        /// <summary> 
        /// Konstruktor klasy Questions, wczytuje wszystkie pytania, i losuje 10
        /// </summary> 
       
        public Questions() {
            this.LoadAllQuestions();
            this.selectQuestionsToGame();
        }

        /// <summary> 
        /// wczytuje wszystkie pytania
        /// </summary> 
        protected void LoadAllQuestions()
        {
            string line;
            
            allQuestionsCounter = 0;
            System.IO.StreamReader file = new System.IO.StreamReader(@"questions.questions");
            while ((line = file.ReadLine()) != null)
            {
                this.allQuestions[this.allQuestionsCounter] = new Question(line);
                this.allQuestionsCounter++;
            }
        }

        /// <summary> 
        /// losuje 10 pytan z bazy wszystkich pytań
        /// </summary> 
        protected void selectQuestionsToGame()
        {

            int[] wylosowane_pytania = new int[500];
            Random rnd = new Random();
            int losowe;
            for (int i = 0; i < 10; i++)
            {
                
                do {
                    losowe = rnd.Next(0, allQuestionsCounter - 1);
                } while (wylosowane_pytania.Contains(losowe));
                this.selectedQuestions[i] = this.allQuestions[losowe];
            }

            /*
            for (int i = 0; i < 10; i++) {
                this.selectedQuestions[i] = this.allQuestions[i];
            }   */     
        }


    }
}
