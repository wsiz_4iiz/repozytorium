﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QUIZ
{
    /// <summary>
    /// Logika interakcji dla klasy gra.xaml
    /// </summary>
    public partial class gra : Window
    {
        public Game game;

        public gra(Game game_)
        {
            this.game = game_;
            InitializeComponent();
            game.wyswietlPytanie(this);


           /* Questions temp = new Questions();
            this.pytanie.Text = temp.selectedQuestions[0].tresc_pytania;
            this.odpowiedz1.Text = temp.selectedQuestions[0].odpowiedz1;
            this.odpowiedz2.Text = temp.selectedQuestions[0].odpowiedz2;
            this.odpowiedz3.Text = temp.selectedQuestions[0].odpowiedz3;
            this.odpowiedz4.Text = temp.selectedQuestions[0].odpowiedz4;*/
        }


        private void bZakonczGre_Click(object sender, RoutedEventArgs e)
        {
            var window = new MainWindow();
            this.Hide();
            window.Show();
        }

        private void bOdpowiedzA_Click(object sender, RoutedEventArgs e)
        {
            game.sprawdzOdpowiedz(1);       
        }

        private void bOdpowiedzB_Click(object sender, RoutedEventArgs e)
        {
            game.sprawdzOdpowiedz(2);
        }

        private void bOdpowiedzC_Click(object sender, RoutedEventArgs e)
        {
            game.sprawdzOdpowiedz(3);
        }

        private void bOdpowiedzD_Click(object sender, RoutedEventArgs e)
        {
            game.sprawdzOdpowiedz(4);
        }
    }
}
