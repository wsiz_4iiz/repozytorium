﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUIZ
{
    public class Question
    {
        public string tresc_pytania;
        public string odpowiedz1;
        public string odpowiedz2;
        public string odpowiedz3;
        public string odpowiedz4;
        public int prawidlowa;

        /// <summary> 
        /// Konstruktor klasy Question, tworzy pytanie, ustawiając odpowiedzi A B C D w losowej kolejności
        /// </summary> 
        /// <param name="questionString">linijka pliku CSV z pytaniem</param>
        public Question(string questionString)
        {
            String pytanie = questionString.Split(';')[0];
            String p1 = questionString.Split(';')[1];
            String p1_ok = questionString.Split(';')[2];
            String p2 = questionString.Split(';')[3];
            String p2_ok = questionString.Split(';')[4];
            String p3 = questionString.Split(';')[5];
            String p3_ok = questionString.Split(';')[6];
            String p4 = questionString.Split(';')[7];
            String p4_ok = questionString.Split(';')[8];

            this.tresc_pytania = pytanie;

            Random rnd2 = new Random();
            int l = rnd2.Next(0, 4);

            if (l == 0) { // 1 2 3 4
                this.odpowiedz1 = p1;
                this.odpowiedz2 = p2;
                this.odpowiedz3 = p3;
                this.odpowiedz4 = p4;
                if (p1_ok == "1") { this.prawidlowa = 1; }
                else if (p2_ok == "1") { this.prawidlowa = 2; }
                else if (p3_ok == "1") { this.prawidlowa = 3; }
                else if (p4_ok == "1") { this.prawidlowa = 4; }
            }
            else if (l == 1) // 2 1 4 3
            {
                this.odpowiedz1 = p2;
                this.odpowiedz2 = p1;
                this.odpowiedz3 = p4;
                this.odpowiedz4 = p3;
                if (p1_ok == "1") { this.prawidlowa = 2; }
                else if (p2_ok == "1") { this.prawidlowa = 1; }
                else if (p3_ok == "1") { this.prawidlowa = 4; }
                else if (p4_ok == "1") { this.prawidlowa = 3; }
            }
            else if (l == 2) //4 1 3 2
            {
                this.odpowiedz1 = p4;
                this.odpowiedz2 = p1;
                this.odpowiedz3 = p3;
                this.odpowiedz4 = p2;
                if (p1_ok == "1") { this.prawidlowa = 4; }
                else if (p2_ok == "1") { this.prawidlowa = 1; }
                else if (p3_ok == "1") { this.prawidlowa = 3; }
                else if (p4_ok == "1") { this.prawidlowa = 2; }
            }
            else if (l == 3) //3 2 1 4
            {
                this.odpowiedz1 = p3;
                this.odpowiedz2 = p2;
                this.odpowiedz3 = p1;
                this.odpowiedz4 = p4;
                if (p1_ok == "1") { this.prawidlowa = 3; }
                else if (p2_ok == "1") { this.prawidlowa = 2; }
                else if (p3_ok == "1") { this.prawidlowa = 1; }
                else if (p4_ok == "1") { this.prawidlowa = 4; }
            }

            else if (l == 4) //4 3 1 2
            {
                this.odpowiedz1 = p4;
                this.odpowiedz2 = p3;
                this.odpowiedz3 = p1;
                this.odpowiedz4 = p2;
                if (p1_ok == "1") { this.prawidlowa = 4; }
                else if (p2_ok == "1") { this.prawidlowa = 3; }
                else if (p3_ok == "1") { this.prawidlowa = 1; }
                else if (p4_ok == "1") { this.prawidlowa = 2; }
            }



        }
    }
}
