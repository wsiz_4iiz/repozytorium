﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QUIZ
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Title = "QUIZ";
        }

        private void bWyjscie_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void bZasady_Click(object sender, RoutedEventArgs e)
        {
            var window = new zasady();
            this.Hide();
            window.Show();
        }

        private void bNowaGra_Click(object sender, RoutedEventArgs e)
        {
            var window = new nick();
            //var window = new gra();
            this.Hide();
            window.Show();
        }

        private void bAutorzy_Click(object sender, RoutedEventArgs e)
        {
            var window = new autorzy();
            this.Hide();
            window.Show();
        }

        private void bRanking_Click(object sender, RoutedEventArgs e)
        {
            var window = new RankingList();
            this.Hide();
            window.Show();
        }
    }
}
