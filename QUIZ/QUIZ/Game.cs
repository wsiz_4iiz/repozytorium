﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Media;

namespace QUIZ
{
   public class Game
    {

        public string nazwaGracza;
        public int punkty;
        public int aktualnePytanie;
        public int czas;
        public Questions pytania_ = new Questions();
        public gra oknoGry;
        public System.Timers.Timer aTimer = new System.Timers.Timer();

        /// <summary> 
        /// Konstruktor klasy Game
        /// </summary> 

        public Game()
        {
            this.aktualnePytanie = 0;
            this.punkty = 0;
            
        }

        /// <summary> 
        /// Wyświetla aktualne pytanie
        /// </summary> 
        /// <param name="okno">okno gry</param>
        public void wyswietlPytanie(gra okno)
        {
            czas = 10;

            //aTimer.Start();
            aTimer.Elapsed += new ElapsedEventHandler(odliczaj);
            aTimer.Interval = 1000;
            aTimer.Enabled = true;

            oknoGry = okno;
            oknoGry.czas.Foreground = Brushes.Black;
            Question aktualne = this.pytania_.selectedQuestions[this.aktualnePytanie];
            okno.pytanie.Text = aktualne.tresc_pytania;
            okno.czas.Content = czas.ToString();
            okno.odpowiedz1.Text = aktualne.odpowiedz1;
            okno.odpowiedz2.Text = aktualne.odpowiedz2;
            okno.odpowiedz3.Text = aktualne.odpowiedz3;
            okno.odpowiedz4.Text = aktualne.odpowiedz4;
            okno.pytania.Content = (aktualnePytanie+1).ToString() + " / 10";
            okno.punkty.Content = punkty.ToString();
            okno.nickg.Content = this.nazwaGracza;
            if (aktualnePytanie == 9) { koniecGry(oknoGry); }
            //aktualnePytanie++;
        }

        /// <summary> 
        /// funkcja uruchamiana przez timer, zmniejsza czas o 1 sekundę, jeśli jest mniej niż 3 sekundy to podświetla czas na czerwono
        /// </summary> 
        private void odliczaj(object source, ElapsedEventArgs e)
        {
            czas--;

            //oknoGry.czas.Content = czas.ToString();
            oknoGry.Dispatcher.Invoke(() => { oknoGry.czas.Content = czas.ToString();});

            if (czas < 4) {
                oknoGry.Dispatcher.Invoke(() => {oknoGry.czas.Foreground = Brushes.Red; });
            }

            if (czas == 0) {
                brakCzasu();
            }

        }

        /// <summary> 
        /// funkcja wywoływana gdy upłynął czas na odpowiedź, zeruje timer i przechodzi do następnego pytania
        /// </summary> 
        public void brakCzasu()
        {
            aTimer.Elapsed -= new ElapsedEventHandler(odliczaj);
            aTimer.Enabled = false;
            aTimer.Stop();
            aktualnePytanie++;
            oknoGry.Dispatcher.Invoke(() => { wyswietlPytanie(oknoGry); });
            //wyswietlPytanie(oknoGry);
        }

        /// <summary> 
        /// Wyświetla aktualne pytanie
        /// </summary> 
        /// <param name="udzielonaOdpowiedz">numer udzielonej odpowiedzi</param>
        public void sprawdzOdpowiedz(int udzielonaOdpowiedz)
        {
            Question aktualne = this.pytania_.selectedQuestions[this.aktualnePytanie];
            aktualnePytanie++;
            aTimer.Elapsed -= new ElapsedEventHandler(odliczaj);
            aTimer.Enabled = false;
            aTimer.Stop();
            if (udzielonaOdpowiedz == aktualne.prawidlowa)
            {
                punkty += 100*czas;   
            }
            
            wyswietlPytanie(oknoGry);
        }

        /// <summary> 
        /// kończy grę
        /// </summary> 
        /// <param name="okno">okno gry</param>
        public void koniecGry(gra okno)
        {
            aTimer.Elapsed -= new ElapsedEventHandler(odliczaj);
            aTimer.Enabled = false;
            aTimer.Stop();
            var window = new koniec(this);
            okno.Hide();
            window.Show();
        }

        



    }
}
