﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUIZ
{
    class RankingPosition
    {
        public string nick;
        public int punkty;
        public int pozycja;

        public RankingPosition(string nick, int punkty, int pozycja)
        {
            this.nick = nick;
            this.punkty = punkty;
            this.pozycja = pozycja;
        }

    }
}
