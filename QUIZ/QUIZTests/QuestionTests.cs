﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUIZ.Tests
{
    [TestClass()]
    public class QuestionTests
    {
        [TestMethod()]
        public void QuestionParseTest()
        {
            String toParse = "Najdłuższa rzeka w Europie to?;Wołga;1;Wisła;0;Dunaj;0;Sekwana;";

            Question test = new Question(toParse);

            String zalozenie = "Najdłuższa rzeka w Europie to?";
            String faktyczny = test.tresc_pytania;
            Assert.AreEqual(zalozenie, faktyczny);
        }
    }
}